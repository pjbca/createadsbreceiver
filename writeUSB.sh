#!/bin/bash

# Peter Burke 11/2/2018
# Based off of: https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/
# and
# https://github.com/Phoenix1747/RouteryPi/blob/master/install.sh

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}


echo "Starting..."

date

umount /media/peter/rootfs; umount /media/peter/boot ; sudo dd bs=4M if=/home/peter/Documents/pi/2019-04-08-raspbian-stretch-lite.img of=/dev/sdf conv=fsync

date

#echo "Cycle power on USB and press enter to continue when USB is remounted."
#read -p "To continue press enter: " out

#cp makeADSBrx.sh /media/peter/rootfs/home/pi/makeADSBrx.sh

#umount /media/peter/rootfs; umount /media/peter/boot


echo "Installation is complete. Install the SD card into your pi for the next step!"
echo "No further action should be required. Closing..."
