# CreateADSBreceiver

from scratch create an ADSB receiver "distro" on raspberry pi with two dongles for 1090  MHz listening

Step one: create SD card with Rasbian on it:
a) Insert USB into a linux computer
b) Run the writeUSB.sh script


```
wget https://gitlab.com/pjbca/createadsbreceiver/raw/master/writeUSB.sh
```
Run script (takes about an hour to run):
```
sudo chmod 777 ~/writeUSB.sh; 
sudo ~/writeUSB.sh | tee buildlog.txt
```

On my machine this take 4.5 minutes.

Step two: Run Raspi-config:
sudo raspi-config
a) Set up wifi
b) Enable ssh login
reboot
sudo reboot now

(Note: If you want, you can log into the Pi from an ssh terminal to continue. Just do an ifconfig and not the IP address, and from the terminal type ssh pi@192.168.1.xxx where xxx is the IP address you found when running ifconfig.)

Alternative to step 2:
Leave SD card in PC, remove/reinsert it.
In boot, touch ssh to create a file named ssh. This will enable ssh login.
In /etc/wpa_supplicant edit the wpa_supplicant.conf file to include the ssid /pwd i.e. add these lines at the end:
network={
    ssid="testing"
    psk="testingPassword"
}
Instructions are here:
https://www.raspberrypi.org/documentation/configuration/wireless/headless.md

Step three: 



Get the script to install and configure the Pi:
```
wget https://gitlab.com/pjbca/createadsbreceiver/raw/master/createADSBrx.sh
```

Run script (takes about an 3.5 min to run):
```
sudo chmod 777 ~/createADSBrx.sh; 
sudo ~/createADSBrx.sh | tee buildlog.txt
```


Need to run ./install.sh
try
 sudo ~/adsb-receiver/install.sh 

