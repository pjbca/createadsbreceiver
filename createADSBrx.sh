#!/bin/bash

# Copyright Peter Burke 6/11/2019

# define functions first


function installstuff {
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Installing a whole bunch of packages..."

    start_time_installstuff="$(date -u +%s)"

    time sudo apt-get -y update # 1 min   #Update the list of packages in the software center                                   
#    time sudo apt-get -y upgrade # 3.5 min
    time sudo apt-get -y install git
#    time sudo apt-get -y install screen # 0.5 min
#    time sudo apt-get -y install tcptrack # 0 min
    time sudo apt-get -y install wget # 0 min
    time sudo apt-get -y install emacs # 4.5 min  (seems you have to run this twice)                                             
    time sudo apt-get -y install emacs # 0 min (seems you have to run this twice)                                             

    echo "Done installing a whole bunch of packages..."



    end_time_installstuff="$(date -u +%s)"
    elapsed_installstuff="$(($end_time_installstuff-$start_time_installstuff))"
    echo "Total of $elapsed_installstuff seconds elapsed for installing packages"
    # 38 mins
    
    
}

function downloadandinstalladsbsoftware {

    start_time_downloadandbuildmavlinkrouter="$(date -u +%s)"
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Downloading git clone of https://github.com/jprochazka/adsb-receiver..."
    #Download the git clone:                                                                                        

    git clone https://github.com/jprochazka/adsb-receiver.git
    cd ~/adsb-receiver
    chmod +x install.sh
    


    
    echo "Done downloading git clone of https://github.com/jprochazka/adsb-receiver.."

    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Start install of adsb software..."

#    ./install.sh

    echo "Done installing adsb software..."

    end_time_downloadandbuildmavlinkrouter="$(date -u +%s)"
    elapsed_downloadandbuildmavlinkrouter="$(($end_time_downloadandbuildmavlinkrouter-$start_time_downloadandbuildmavlinkrouter))"
    echo "Total of $elapsed_downloadandbuildmavlinkrouter seconds elapsed for downloading and building mavlink router"
    # 13 min

}

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}

#***********************END OF FUNCTION DEFINITIONS******************************

set -x

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will install adsb receiver software."
echo "See README in  gitlab CreateADSBreceiver repo for documentation."


echo "Starting..."

date


start_time="$(date -u +%s)"

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

installstuff


downloadandinstalladsbsoftware

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

date

end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for the entire process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."

